package com.cafetown.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Properties;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BasePage extends UtilPage {

	public static WebDriver driver = null;
	public static Properties prop;

	public BasePage() {
		try {
			prop = new Properties();
			FileInputStream objFile = new FileInputStream(System.getProperty("user.dir")
					+ "\\src\\main\\java\\com\\cafetown\\config\\Configurations.properties");
			prop.load(objFile);
		} catch (IOException e) {
			e.getMessage();
		}
	}

	@Before
	public static void browserLaunch() {
		String browserName = prop.getProperty("browser");
		if (browserName.equalsIgnoreCase("chrome")) {
			System.setProperty("webdriver.chrome.driver", "src\\test\\resources\\drivers\\chromedriver.exe");
			driver = new ChromeDriver();
		} else if (browserName.equalsIgnoreCase("firefox")) {
			System.setProperty("webdriver.gecko.driver", "src\\test\\resources\\drivers\\geckodriver.exe");
			driver = new FirefoxDriver();
		}
		driver.manage().window().maximize();
		driver.manage().timeouts().pageLoadTimeout(UtilPage.page_Time_Out, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(UtilPage.implicity_Time, TimeUnit.SECONDS);
		driver.get(prop.getProperty("url"));
	}

	@After
	public static void closeBrowser() {
		driver.close();
		driver.quit();
	}

	public void waitUntilElementIsStable(WebElement element) throws Exception {
		new WebDriverWait(driver, 60).until(ExpectedConditions.visibilityOf(element));
	}

	public void click(WebElement element) throws Throwable {
		// TODO Auto-generated method stub
		waitUntilElementIsStable(element);
		element.click();
	}

	public void enterTextFieldValue(WebElement element, String txt) throws Throwable {
		click(element);
		element.clear();
		element.sendKeys(txt);
		element.sendKeys(Keys.RIGHT);
		element.sendKeys(Keys.TAB);
	}

	public void waitUntilAllElementIsStable(List<WebElement> element) throws Exception {
		new WebDriverWait(driver, 60).until(ExpectedConditions.visibilityOfAllElements(element));
	}

	public static String getUniqueEmployeeName() {
		String charSet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
		String fname = getRandomString(charSet, 3);
		String lname = getRandomString(charSet, 3);
		return "_Auto" + fname + "," + "Auto" + lname;
	}

	public static String getRandomString(String charSet, int length) {
		char[] text = new char[length];

		for (int i = 0; i < length; i++) {
			text[i] = charSet.charAt(new Random().nextInt(charSet.length()));
		}
		return new String(text);
	}
	
	public static void acceptPopup() {
		driver.switchTo().alert().accept();
	}
	

}
