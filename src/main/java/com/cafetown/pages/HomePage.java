package com.cafetown.pages;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.cafetown.utils.BasePage;

//import junit.framework.Assert;

public class HomePage extends BasePage {
	
	public HomePage() throws IOException {
		PageFactory.initElements(driver, this);
		// TODO Auto-generated constructor stub
	}
	
	@FindBy(css = "#employee-list li")
	private List<WebElement> employeeList;
	
	@FindBy(id = "bAdd")
	private WebElement createButton;

	@FindBy(id = "bEdit")
	private WebElement editButton;
	
	@FindBy(id = "bDelete")
	private WebElement deleteButton;
	
	@FindBy(css = "p[ng-click = 'logout()']")
	private WebElement logoutButton;
	
	public boolean isEmployeeListsExist() throws Throwable {
		waitUntilAllElementIsStable(employeeList);
		boolean flag = false;
		if (employeeList.size() > 0) {
			flag = true;
			return flag;
		} else return flag;
	}
	
	public void selectOption(String value) throws Throwable {
		waitUntilAllElementIsStable(employeeList);
		switch (value) {
		case "Create":
			click(createButton);
        break;
		case "Edit":
			click(editButton);
        break;
		case "Delete":
			click(deleteButton);
        break;
		default: System.out.println("unknown test");
		}
	}
	
	public boolean verifyEmployeeIsExist(String employeeName) throws Throwable {
		waitUntilAllElementIsStable(employeeList);
		boolean status = false;
		for (int i = 1; i <= 10
				; i++) {
			if (employeeList.get(i).getText().trim().equalsIgnoreCase(employeeName)) {
				click(employeeList.get(i));
				status = true;
				return status;
			}
			if (i == employeeList.size()) {
				status = false;
				break;
			}
		}
		return status;
	}

	public boolean verifyAndSelectEmployeeFromList(String empName) throws Throwable {
		boolean flag = false;
		waitUntilAllElementIsStable(employeeList);
		for (int i = 0; i <= employeeList.size(); i++) {
			if (employeeList.get(i).getText().trim().equalsIgnoreCase(empName)) {
				click(employeeList.get(i));
				flag = true;
				Actions act = new Actions(driver);
				act.doubleClick(employeeList.get(i)).perform();
				break;
			}
			if (i == employeeList.size()) {
				flag = false;
			}
		}
		return flag;
	}
	
	public void selectRandomEmployee() throws Throwable {
		Random rand = new Random();
		int randomNo = rand.nextInt(employeeList.size());
		click(employeeList.get(randomNo));
	}
	
	public void dblClkEmployee() throws Throwable {
		Random rand = new Random();
		int randomNo = rand.nextInt(employeeList.size());
		click(employeeList.get(randomNo));
		Actions act = new Actions(driver);
		act.doubleClick(employeeList.get(randomNo)).build().perform();
	}
	
	public void clickLogout() throws Throwable {
		click(logoutButton);
		driver.quit();
	}
	
	public boolean verifyEmployee(String employeeName) throws Throwable {
		waitUntilAllElementIsStable(employeeList);
		boolean status = true;
		List<String> emp = new ArrayList<String>();
		for (int i = 0; i < employeeList.size(); i++) {
			emp.add(employeeList.get(i).getText());
		}
		
		System.out.println("Array Size: " +emp.size() );
		for (int j = 0; j < emp.size(); j++ ) {
			System.out.println(j+".Array Text : " +emp.get(j));
			if (emp.get(j).equalsIgnoreCase(employeeName)) {
				status = false;
				break;
		}
	}
		return status;
	}
	
	
}
