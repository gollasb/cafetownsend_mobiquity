package com.cafetown.pages;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.cafetown.utils.BasePage;

public class EmployeeDetailsPage extends BasePage {
	
	public EmployeeDetailsPage() throws IOException {
		PageFactory.initElements(driver, this);
		// TODO Auto-generated constructor stub
	}
	
	@FindBy(css = "label span")
	private List<WebElement> employeeDetaillabels;
	
	@FindBy(xpath = "//label/span[text() = 'First name:']/following-sibling::input")
	private WebElement firstNameInput;
	
	@FindBy(xpath = "//label/span[text() = 'Last name:']/following-sibling::input")
	private WebElement lastNameInput;
	
	@FindBy(xpath = "//label/span[text() = 'Start date:']/following-sibling::input")
	private WebElement startDateInput;
	
	@FindBy(css = "input[type = 'email']")
	private WebElement emailInput;
	
	@FindBy(css = ".formFooter button + button")
	private WebElement addButton;
	
	@FindBy(css = ".formFooter button")
	private WebElement updateButton;
	
	@FindBy(css = ".formFooter p")
	private WebElement deleteButton;
	
	@FindBy(css = "p[ng-click = 'logout()']")
	private WebElement logoutButton;
	
	@FindBy(css = "a[class = 'subButton bBack']")
	private WebElement backButton;
	
	public void verifyEmployeeDetailsLabel(List<String> data) throws Throwable {
		waitUntilAllElementIsStable(employeeDetaillabels);
		int i = 0;
		for (WebElement employee : employeeDetaillabels) {
			String emp = employee.getText().split(":")[0].trim();
			Assert.assertTrue(emp.equalsIgnoreCase(data.get(i)));
			i++;
		}
	}
	
	public String getFirstNameValue() {
		return firstNameInput.getAttribute("value").trim();
	}
	
	public String getLastNameValue() {
		return lastNameInput.getAttribute("value").trim();
	}
	
	public String getStartDateValue() {
		return startDateInput.getAttribute("value").trim();
	}
	
	public String getEmailValue() {
		return emailInput.getAttribute("value").trim();
	}
	
	public void clickAdd() throws Throwable {
		click(addButton);
	}
	
	public void clickUpdate() throws Throwable {
		click(updateButton);
	}
	
	public void clickDelete() throws Throwable {
		click(deleteButton);
	}
	
	public void clickLogout() throws Throwable {
		click(logoutButton);
	}
	
	public void clickBack() throws Throwable {
		click(backButton);
	}
	
	public void inputEmployeeDetails() throws Throwable {
		enterTextFieldValue(firstNameInput, getUniqueEmployeeName().split(",")[0].trim());
		enterTextFieldValue(lastNameInput, getUniqueEmployeeName().split(",")[1].trim());
		enterTextFieldValue(startDateInput, "2019-11-20");
		enterTextFieldValue(emailInput, "SureshG@gmail.com");
	}
	
	public List<String> getEmployeesValues() {
		List<String> empList=new ArrayList<String>();
		empList.add(firstNameInput.getAttribute("value").trim());
		empList.add(lastNameInput.getAttribute("value").trim());
		empList.add(startDateInput.getAttribute("value").trim());
		empList.add(emailInput.getAttribute("value").trim());
		return empList;

	}
	
	
	
}
