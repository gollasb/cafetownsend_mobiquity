package com.cafetown.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.cafetown.utils.BasePage;

public class LoginPage extends BasePage {
	

	@FindBy(css = "input[type = 'text']")
	public WebElement userName;
	
	@FindBy(css = "input[type = 'password']")
	public WebElement password;
	
	@FindBy(css = "button[type = 'submit']")
	public WebElement login;
	
	public LoginPage() throws Throwable {
		PageFactory.initElements(driver, this);
		// TODO Auto-generated constructor stub
	}

	 public void login(String un, String pwd) throws Throwable {
		 Thread.sleep(5000);
		 waitUntilElementIsStable(userName);
		 enterTextFieldValue(userName, un);
		 enterTextFieldValue(password, pwd);
		 click(login);
	 }
	

}
