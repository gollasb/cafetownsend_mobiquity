@CafeTown
Feature: Cafe Townsend fucntionalities
	In order to create, Edit, Delete the Employees
	Login with Luke
	and verify the functionalities
	
Background: 
	Given I login as an "Luke" user
	Then List of Employees should be displayed

Scenario: Verify the functionities of Create,Edit,Delete the Employee Details
	When User selects any of employees from the list
	Then following details should be present
		| First name |
		| Last name	 |
		| Start date |
		| Email		 |
 #Creating a new Employee
	When User clicks on "Create" Button
	And Fills all the employee details
	Then Employee should be added in the list
#Editing Existing Employee
	And User clicks on "Edit" Button
	And Update any values
	And Verify edited Employee from the list
	Then Edited values should be displyed for that Employee	
# Deleting Existing Employee
	When User clicks on Delete Button
	Then Deleted Employee should not be present in the list	
