package com.cafetown.steps;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
//import org.openqa.selenium.WebElement;

import com.cafetown.pages.EmployeeDetailsPage;
import com.cafetown.pages.HomePage;
import com.cafetown.pages.LoginPage;
import com.cafetown.utils.BasePage;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.api.DataTable;
import cucumber.api.java.en.Given;

/**
 * @author suresh
 *Step definition file, which contains the entire test script to login into the application, Add, Edit and Delete Employees in CafeTownsend.
 */
public class CafeTown extends BasePage {

	LoginPage loginPage;
	HomePage homePage;

	public String firstName;
	public String lastName;
	public String startDate;
	public String email;
	List<String> actualElements = new ArrayList<String>();

	
	@Given("^I login as an \"(.*?)\" user$")
	public void I_Login_as_an_luke_user(String user) throws Throwable {
		BasePage.browserLaunch();
		loginPage = new LoginPage();
		loginPage.login(prop.getProperty("userName"), prop.getProperty("password"));
	}

	@Then("^List of Employees should be displayed$")
	public void list_of_Employees_should_be_displayed() throws Throwable {
		homePage = new HomePage();
		Assert.assertTrue(homePage.isEmployeeListsExist());
		homePage.selectRandomEmployee();
	}

	@When("^User selects any of employees from the list$")
	public void user_selects_any_of_employees_from_the_list() throws Throwable {
		homePage.dblClkEmployee();
	}

	/**
	 *method used to verify the Employee Details
	 */
	@Then("^following details should be present$")
	public void following_details_should_be_present(DataTable label) throws Throwable {
		EmployeeDetailsPage empDetailsPage = new EmployeeDetailsPage();
		List<String> data = label.asList(String.class);
		empDetailsPage.verifyEmployeeDetailsLabel(data);
		empDetailsPage.clickBack();
	}


	@When("^User clicks on \"([^\"]*)\" Button$")
	public void user_clicks_on_Button(String label) throws Throwable {
		homePage = new HomePage();
		homePage.selectOption(label);
	}

	/**
	 * Method used to Enter the Employee details
	 */
	@When("^Fills all the employee details$")
	public void fills_all_the_employee_details() throws Throwable {
		EmployeeDetailsPage empDetailsPage = new EmployeeDetailsPage();
		empDetailsPage.inputEmployeeDetails();
		firstName = empDetailsPage.getFirstNameValue();
		lastName = empDetailsPage.getLastNameValue();
		empDetailsPage.clickAdd();
	}

	/**
	 * Verify the Newly added employees in the list
	 */
	@Then("^Employee should be added in the list$")
	public void employee_should_be_added_in_the_list() throws Throwable {
		System.out.println("Employee name: " + firstName + " " + lastName);
		Assert.assertTrue(homePage.verifyEmployeeIsExist(firstName + " " + lastName));
	}

	/**
	 * Update Employee details 
	 */
	@When("^Update any values$")
	public void update_any_values() throws Throwable {
		EmployeeDetailsPage empDetailsPage = new EmployeeDetailsPage();
		empDetailsPage.inputEmployeeDetails();
		firstName = empDetailsPage.getFirstNameValue();
		lastName = empDetailsPage.getLastNameValue();
		startDate = empDetailsPage.getStartDateValue();
		email = empDetailsPage.getEmailValue();
		empDetailsPage.clickUpdate();
	}

	/**
	 * Verify Updated Employee details in the list
	 */
	@Then("^Verify edited Employee from the list$")
	public void double_click_on_edited_Employee_from_the_list() throws Throwable {
		homePage = new HomePage();
		System.out.println("Employ: " + firstName+" "+lastName);
		Assert.assertTrue(homePage.verifyAndSelectEmployeeFromList(firstName+" "+lastName));
	}

	@Then("^Edited values should be displyed for that Employee$")
	public void edited_values_should_be_displyed_for_that_Employee() throws Throwable {
		EmployeeDetailsPage empDetailsPage = new EmployeeDetailsPage();
		actualElements = empDetailsPage.getEmployeesValues();
		Assert.assertEquals(firstName, actualElements.get(0));
		Assert.assertEquals(lastName, actualElements.get(1));
		Assert.assertEquals(startDate, actualElements.get(2));
		Assert.assertEquals(email, actualElements.get(3));
	}
	
	/**
	 Clicking on Delete Button
	 */
	@When("^User clicks on Delete Button$")
	public void User_clicks_on_Delete_Button() throws Throwable {
		EmployeeDetailsPage empDetailsPage = new EmployeeDetailsPage();
		empDetailsPage.clickDelete();
		BasePage.acceptPopup();
		
	}

	/**
	 * Verification of Deleted Employee in the list
	 */
	@When("^Deleted Employee should not be present in the list$")
	public void deleted_Employee_should_not_be_present_in_the_list() throws Throwable {
		Assert.assertFalse(homePage.verifyEmployeeIsExist(firstName+" "+lastName));
		homePage.clickLogout();
	}
}
