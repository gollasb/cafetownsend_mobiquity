package com.cafetown.bdd;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

/**
 * @author suresh
 * Main class, TestRunnersuite to run the script
 * Run this file as Junit test to execute the scenarios.
 */
@RunWith(Cucumber.class)
@CucumberOptions(
		features = "src/test/resources/features",
		glue = "com/cafetown/steps",
		tags ="@CafeTown",
		plugin = {"html:target/cucmber-html-report"}
		)
public class TestRunnerSuite {}