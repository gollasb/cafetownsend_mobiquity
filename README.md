# Assignment - Mobiquity
Creation of Autoamated testcases for CafeTownSend application using Selenium Cucumber BDD tool

�	General Info

The CafeTownsend application allows the user to login into the application and perform below functionalities
	
		- Viewing existing Employees
		- Adding new Employees
		- Update existing Employees
		- Delete Employees 
�	Framework

All the scenarios are automated using Selenium with Cucumber (BDD Framework) , which uses simple Gherkin language to write the tests using feature file,  test runner and step definition files.

Creating this project using Maven
  Project Name: Cafetown  
  
						        |-->Src/main/java -- Config pages, Pages, Utils
							    |-->src/test/java ---- Test runner suite file and Step Definition file
							    |--> src/test/resources ---Feature file(Manual tests) & Drivers
							    |--> target/cucumer-html-report -- Result reporting in HTML  									
  
�	Setup

To Run this project

	-Export the project in any IDE, Eclipse/IntelliJ
	-Run Maven update project
	-Goto cafetown\src\test\java\com\cafetown\bdd
	-Run TestRunnersuite.Java file 
	
To view the results 

	- Navigate to cafetown\target\cucmber-html-report
	- select index.html, right click and open with webrowser to view the results in HTML






